# StepMania stop chart creator

This tool will create a "stop chart" from an existing StepMania chart. The arrows appear evenly-spaced and the chart stops on each arrow ([example](https://youtu.be/OPNRaaeFPPU)). The chart shows what arrows to press but not the timing of the arrows. This adds another layer of difficulty to the game as the player is required to know the correct timing from memory or existing familiarity with the chart, but is not as challenging as playing a chart completely blind.

## How to use

This is a Python script designed to run on Python 3.x. If you do not have Python you will need to install it first.

The syntax is as follows:

**Linux:** `./stop_chart.py infile outfile [scroll speed]`

**Windows (command prompt):** `stop_chart.py infile outfile [scroll speed]`

`infile` is the file path to the original chart that you want to use. This can be either a `.sm` or a `.ssc` chart. This file will not be modified.

`outfile` is the file path where you want to create the modified chart. The modified chart will be written in `.ssc` format so you should use the appropriate extension.

`scroll speed` controls how quickly the chart moves from one arrow to the next between stopping, allowing for a more visually pleasing effect. The parameter is expressed as the time taken in seconds for each arrow to reach the top of the screen. This parameter is optional and if omitted the chart will jump instantly from one arrow to the next.

Example:

**Linux:** `./stop_chart.py "/usr/local/stepmania-5.1/Songs/StepMania 5/Goin' Under/Goin' Under.ssc" "~/.stepmania-5.1/Songs/My Stop Charts/Goin' Under/Goin' Under_stop_chart.ssc" 0.1`

**Windows (command prompt):** `stop_chart.py "C:\Games\StepMania 5.1\Songs\Goin' Under\Goin' Under.ssc" "C:\Games\StepMania 5.1\Songs\My Stop Charts\Goin' Under\Goin' Under_stop_chart.ssc" 0.1`

The file paths shown are an example only, make sure to use the appropriate paths for your installation. The output directory needs to be created beforehand. Do not put the modified chart in the same directory as the original chart.

After creating the modified chart, you will need to manually copy all of the media/assets (music, banner, background, etc.) to the output directory.

## Limitations/TODO

* Some less common chart types (couple, routine, etc.) are not supported.

* Charts that use warps or negative BPMs are not supported. Charts with regular BPM changes work as expected. Charts with scroll speed gimmicks or stops/freezes are supported but the effects are lost.

* Video backgrounds, or static backgrounds that change during the song, are lost. This is because the way that backgrounds are listed in the chart file makes it complicated to adjust the background timing to match the modified chart.

* Charts with fake segments are not supported for the same reason as video backgrounds (individual fake notes work as expected).

* Charts with time signature changes are not supported because there is no documentation on what these do.

* Anything involving keysounds is ignored. Charts with keysounds may result in undefined behavior.

* The output chart BPM does not correlate with the music. This doesn't affect the gameplay but causes visual effects such as the life bar to appear out-of-sync with the music.

* Long charts may drift out-of-sync by a few milliseconds due to rounding errors.

## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
