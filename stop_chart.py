#!/usr/bin/env python3

import sys
import re

stepstypes = { "dance-single": 4, "dance-double": 8, "dance-solo": 6, "dance-threepanel": 3, "pump-single": 5, "pump-halfdouble": 6, "pump-double": 10 }
difficulties = ["Beginner", "Easy", "Medium", "Hard", "Challenge", "Edit"]

class InvalidChartFileError(Exception):
    def __init__(self, message):
        self.message = message

class UnacceptableChartError(Exception):
    def __init__(self, message):
        self.message = message

# Parse a chart file into a list of statements
def read_chart(filename):
    chart = []

    statement = ""
    with open(filename, "rt") as file:
        for line in file:
            line = re.sub("//.*$", "", line).strip()
            if len(line) == 0 or line.startswith("//"):
                continue

            # TODO: how is a semicolon in a text string (title etc.) handled?
            for part in [part + ";" for part in line.split(";")[:-1]] + [line.split(";")[-1]]:
                statement += part

            if statement.endswith(";"):
                if not statement.startswith("#") or not ":" in statement:
                    raise InvalidChartFileError("Chart file is malformed")

                matches = re.findall("^#([^:]*):(.*);$", statement)
                name = matches[0][0]
                value = matches[0][1]
                raw_value = value

                def recursive_split(value, separator):
                    if type(value) is list:
                        return [recursive_split(value, separator) for value in value]
                    else:
                        if separator in value:
                            return value.split(separator)
                        else:
                            return value
                value = recursive_split(value, ":")
                value = recursive_split(value, ",")
                value = recursive_split(value, "=")
                del recursive_split

                index = len(chart)
                chart += [(name, value, raw_value, index)]    # index is included so that chart.index() works as expected when there are identical statements in the chart file (TODO: find a more elegant way to do this)
                statement = ""

    return chart

# Find a named parameter in a list of chart statements
def find_parameter(name, statements):
    for statement in statements:
        if statement[0] == name:
            return statement
    return None

# Find the closest occurrence of a named parameter before a given index (non-inclusive)
def find_previous(name, chart, index):
    return find_parameter(name, reversed(chart[:index]))

# Find the closest occurrence of a named parameter after a given index (non-inclusive)
def find_next(name, chart, index):
    return find_parameter(name, chart[index + 1:])

# Find a named parameter in a chart file, using the given notedata block positions to allow per-note-list parameters in .ssc charts
def find_ssc_parameter(name, chart, header_end, notedata_start, notedata_end):
    parameter = None
    if not notedata_start == None and not notedata_end == None:
        parameter = find_parameter(name, chart[notedata_start:notedata_end])
    if parameter == None:
        parameter = find_parameter(name, chart[:header_end])
    return parameter

# Return the index of the first non-header statement in a chart
def get_header_end(chart):
    return chart.index(find_next("NOTEDATA", chart, -1) or find_next("NOTES", chart, -1))

# Get a list of all the notes lists in a chart
def get_notes(chart):
    notes = []

    header_end = get_header_end(chart)
    for statement in chart:
        if statement[0] == "NOTEDATA":
            # chart is .ssc
            notedata_start = chart.index(statement)
            next_notedata_statement = find_next("NOTEDATA", chart, chart.index(statement))
            notedata_end = chart.index(next_notedata_statement) if not next_notedata_statement == None else len(chart)

            notes_statement = find_ssc_parameter("NOTES", chart, header_end, notedata_start, notedata_end)
            if notes_statement == None:
                raise InvalidChartFileError(".ssc missing NOTES for NOTEDATA")
            stepstype_statement = find_ssc_parameter("STEPSTYPE", chart, header_end, notedata_start, notedata_end)
            if stepstype_statement == None:
                raise InvalidChartFileError(".ssc missing STEPSTYPE for NOTEDATA")
            difficulty_statement = find_ssc_parameter("DIFFICULTY", chart, header_end, notedata_start, notedata_end)
            if difficulty_statement == None:
                raise InvalidChartFileError(".ssc missing DIFFICULTY for NOTEDATA")
            meter_statement = find_ssc_parameter("METER", chart, header_end, notedata_start, notedata_end)
            if meter_statement == None:
                raise InvalidChartFileError(".ssc missing METER for NOTEDATA")

            notes += [{ "notes": notes_statement[1], "stepstype": stepstype_statement[1], "difficulty": difficulty_statement[1], "meter": int(meter_statement[1]), "notedata_start": notedata_start, "notedata_end": notedata_end }]
        elif statement[0] == "NOTES" and not re.match("^[0-9]", statement[1][0]):
            # chart is .sm
            notes += [{ "notes": statement[1][5], "stepstype": statement[1][0], "difficulty": statement[1][2], "meter": int(statement[1][3]), "notedata_start": None, "notedata_end": None }]

    return notes

# Return a list of note events with timestamps from a chart notes list as returned by get_notes(chart)
def convert_notes(chart, notes_list):
    header_end = get_header_end(chart)
    notedata_start = notes_list["notedata_start"]
    notedata_end = notes_list["notedata_end"]

    # Find the BPM list

    bpm_statement = find_ssc_parameter("BPMS", chart, header_end, notedata_start, notedata_end)
    if bpm_statement == None:
        raise InvalidChartFileError("Chart file is missing BPMS")
    if type(bpm_statement[1][0]) is list:
        bpm_list = bpm_statement[1]
    else:
        bpm_list = [bpm_statement[1]]

    for bpm in bpm_list:
        if float(bpm[1]) <= 0:
            raise UnacceptableChartError("Chart has negative BPM")

    # Find the stops and delays lists

    stops_statement = find_ssc_parameter("STOPS", chart, header_end, notedata_start, notedata_end)
    if not stops_statement == None and not len(stops_statement[1]) == 0:
        if type(stops_statement[1][0]) is list:
            stops_list = stops_statement[1]
        else:
            stops_list = [stops_statement[1]]
    else:
        stops_list = None

    delays_statement = find_ssc_parameter("DELAYS", chart, header_end, notedata_start, notedata_end)
    if not delays_statement == None and not len(delays_statement[1]) == 0:
        if type(delays_statement[1][0]) is list:
            delays_list = delays_statement[1]
        else:
            delays_list = [delays_statement[1]]
    else:
        delays_list = None

    # Check that there are no unsupported gimmicks

    def check_gimmick(name):
        statement = find_ssc_parameter(name, chart, header_end, notedata_start, notedata_end)
        return statement == None or len(statement[1]) == 0
    for gimmick in ["WARPS", "FAKES"]:
        if not check_gimmick(gimmick):
            raise UnacceptableChartError("Chart uses " + gimmick)
    del check_gimmick
    timesignatures_statement = find_ssc_parameter("TIMESIGNATURES", chart, header_end, notedata_start, notedata_end)
    if not timesignatures_statement == None and not len(timesignatures_statement[1]) == 0:
        if not (len(timesignatures_statement[1]) == 3 and type(timesignatures_statement[1][0] is str) and float(timesignatures_statement[1][0]) == 0.0 and timesignatures_statement[1][1] == "4" and timesignatures_statement[1][2] == "4"):
            raise UnacceptableChartError("Chart uses TIMESIGNATURES")

    # Construct a list of notes with timestamps

    notes = []
    current_beat = 0    # beats will be counted in 1/48 of a quarter note (1/192 of a measure)
    current_time = 0.0    # time will be counted in seconds
    def get_current_bpm():
        current_bpm = 60.0
        for bpm in bpm_list:
            if float(bpm[0]) <= current_beat / 48.0:
                current_bpm = float(bpm[1])
        return current_bpm

    columns = stepstypes[notes_list["stepstype"]]
    for measure in notes_list["notes"]:
        beat_count = len(measure) // columns
        if not len(measure) % columns == 0:
            raise InvalidChartFileError("Measure has incorrect number of notes")
        if not beat_count in [4, 8, 12, 16, 24, 32, 48, 64, 192]:
            raise InvalidChartFileError("Measure has incorrect number of beats")

        for beat in range(beat_count):
            if not delays_list == None:
                for delay in delays_list:
                    if round(float(delay[0]) * 48.0) == current_beat:
                        current_time += float(delay[1])

            for column in range(columns):
                if not measure[column::columns][beat] == "0":
                    notes += [("".join([measure[column::columns][beat] for column in range(columns)]), current_time)]
                    break

            if not stops_list == None:
                for stop in stops_list:
                    if round(float(stop[0]) * 48.0) == current_beat:
                        current_time += float(stop[1])

            current_time += (60.0 / get_current_bpm()) * (4.0 / beat_count)
            current_beat += 192 // beat_count

    del get_current_bpm

    return notes

# Write the header fields to a chart file
def write_header(src_chart, dst_file):
    header_end = get_header_end(src_chart)

    def write_literal_parameter(name, value):
        dst_file.write("#" + name + ":" + value + ";\n")
    def write_copied_parameter(name):
        statement = find_ssc_parameter(name, src_chart, header_end, None, None)
        if not statement == None:
            assert statement[0] == name
            write_literal_parameter(name, statement[2])

    write_copied_parameter("VERSION")
    write_copied_parameter("TITLE")
    write_copied_parameter("SUBTITLE")
    write_copied_parameter("ARTIST")
    write_copied_parameter("TITLETRANSLIT")
    write_copied_parameter("SUBTITLETRANSLIT")
    write_copied_parameter("ARTISTTRANSLIT")
    write_copied_parameter("GENRE")
    write_copied_parameter("ORIGIN")
    write_copied_parameter("BANNER")
    write_copied_parameter("BACKGROUND")
    write_copied_parameter("PREVIEWVID")
    write_copied_parameter("JACKET")
    write_copied_parameter("CDIMAGE")
    write_copied_parameter("DISCIMAGE")
    write_copied_parameter("LYRICSPATH")
    write_copied_parameter("CDTITLE")
    write_copied_parameter("MUSIC")
    write_copied_parameter("PREVIEW")
    write_copied_parameter("OFFSET")
    write_copied_parameter("SAMPLESTART")
    write_copied_parameter("SAMPLELENGTH")
    write_literal_parameter("SELECTABLE", "YES")
    displaybpm_statement = find_ssc_parameter("DISPLAYBPM", src_chart, header_end, None, None)
    if not displaybpm_statement == None:
        write_literal_parameter("DISPLAYBPM", displaybpm_statement[2])
    else:
        bpm_statement = find_ssc_parameter("BPMS", src_chart, header_end, None, None)
        if not bpm_statement == None:
            if type(bpm_statement[1][0]) is list:
                bpm_list = bpm_statement[1]
            else:
                bpm_list = [bpm_statement[1]]
            write_literal_parameter("DISPLAYBPM", min(bpm[1] for bpm in bpm_list) + ":" + max(bpm[1] for bpm in bpm_list))
    write_copied_parameter("LASTSECONDHINT")

# Write a notedata block to a chart file
def write_notedata(src_chart, src_notedata_start, src_notedata_end, dst_file, notes, stepstype, difficulty, meter, scroll_duration):
    header_end = get_header_end(src_chart)

    def write_literal_parameter(name, value):
        dst_file.write("#" + name + ":" + value + ";\n")
    def write_copied_parameter(name):
        statement = find_ssc_parameter(name, src_chart, header_end, src_notedata_start, src_notedata_end)
        if not statement == None:
            assert statement[0] == name
            write_literal_parameter(name, statement[2])

    write_literal_parameter("NOTEDATA", "")
    write_literal_parameter("STEPSTYPE", stepstype)
    write_literal_parameter("DIFFICULTY", difficulty)
    write_literal_parameter("METER", str(meter))
    displaybpm_statement = find_ssc_parameter("DISPLAYBPM", src_chart, header_end, src_notedata_start, src_notedata_end)
    if not displaybpm_statement == None:
        write_literal_parameter("DISPLAYBPM", displaybpm_statement[2])
    else:
        bpm_statement = find_ssc_parameter("BPMS", src_chart, header_end, src_notedata_start, src_notedata_end)
        if not bpm_statement == None:
            if type(bpm_statement[1][0]) is list:
                bpm_list = bpm_statement[1]
            else:
                bpm_list = [bpm_statement[1]]
            write_literal_parameter("DISPLAYBPM", min(bpm[1] for bpm in bpm_list) + ":" + max(bpm[1] for bpm in bpm_list))
    write_copied_parameter("CREDIT")

    note_time_deltas = [notes[index][1] - notes[index - 1][1] if index > 0 else notes[0][1] for index in range(len(notes))]
    if not scroll_duration == None:
        bpms_values = [str(index - 1) + ".0=" + str(60.0 / min(note_time_deltas[index], scroll_duration)) for index in range(1, len(notes))]
        write_literal_parameter("BPMS", ",".join(bpms_values))
        delays_values = [str(index) + ".0=" + str(note_time_deltas[index] - scroll_duration if index > 0 else note_time_deltas[index]) for index in range(len(notes)) if index == 0 or note_time_deltas[index] > scroll_duration]
        write_literal_parameter("DELAYS", ",".join(delays_values))
    else:
        write_literal_parameter("BPMS", "0.0=60.0")
        delay_values = [str(index) + ".0=" + str(note_time_deltas[index]) for index in range(len(notes))]
        write_literal_parameter("DELAYS", ",".join(delay_values))
        warps_values = [str(index) + ".0=1.0" for index in range(len(notes))]
        write_literal_parameter("WARPS", ",".join(warps_values))
    notes_string = ""
    beat_count = 0
    for beat in notes[:-1]:
        notes_string += beat[0] + "\n"
        beat_count += 1
        if beat_count == 4:
            notes_string += ",\n"
            beat_count = 0
    notes_string += notes[-1][0] + "\n"
    beat_count += 1
    while not beat_count == 4:
        notes_string += "0000\n"
        beat_count += 1
    write_literal_parameter("NOTES", notes_string)

# Get arguments

src_filename = sys.argv[1]
dst_filename = sys.argv[2]
if len(sys.argv) >= 4:
    scroll_duration = float(sys.argv[3])
else:
    scroll_duration = None

# Parse chart

try:
    src_chart = read_chart(src_filename)
    src_notes = get_notes(src_chart)
except (InvalidChartFileError, UnacceptableChartError) as error:
    print("Error reading chart: %s", error.message)
    sys.exit(1)
else:
    print("Read chart")

# Convert notes

dst_notes = []
for notes in src_notes:
    if notes["stepstype"] in stepstypes:
        try:
            dst_notes += [(convert_notes(src_chart, notes), notes)]
        except (InvalidChartFileError, UnacceptableChartError) as error:
            print("Skipping %s %s: %s" % (notes["stepstype"], notes["difficulty"], error.message))
        else:
            print("Converted %s %s" % (notes["stepstype"], notes["difficulty"]))
    else:
        print("Skipping %s %s: Unsupported stepstype" % (notes["stepstype"], notes["difficulty"]))

# Write out the chart

if len(dst_notes) > 0:
    with open(dst_filename, "wt") as dst_file:
        write_header(src_chart, dst_file)
        for notes in dst_notes:
            write_notedata(src_chart, notes[1]["notedata_start"], notes[1]["notedata_end"], dst_file, notes[0], notes[1]["stepstype"], notes[1]["difficulty"], notes[1]["meter"], scroll_duration)
    print("Written output chart")
else:
    print("Output chart has no notes, not writing empty chart")
